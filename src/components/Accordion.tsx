import React, { useState } from 'react';


type props = {
  title: string,
};

const Accordion: React.FC<props> = ({ title, children }) => {
  const [open, setOpen] = useState(false);
  const maxHeight = open ? React.Children.count(children) * 200 : 0;
  return (
    <div className="accordion rehnen-accordion">
      <input
        type="checkbox"
        id={`accordion-1-${title}`}
        name="accordion-checkbox"
        onClick={() => setOpen((prevOpen) => !prevOpen)}
        hidden
      />
      <label className="accordion-header" htmlFor={`accordion-1-${title}`}>
        <span className="accordion-title">
          {title}
        </span>
        <i className="icon icon-arrow-right mr-1" />
      </label>
      <div className="accordion-body" style={{ maxHeight }}>
        {children}
      </div>
    </div>
  );
};

export default Accordion;
