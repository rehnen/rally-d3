import React from 'react';


type ToolboxProps = {
  onDelete: () => void,
};

const LineToolbox: React.FC<ToolboxProps> = ({
  onDelete,
}) => (
  <div className="toolbox container">
    <div className="column col-3 col-mx-auto toolbox-main-area">
      <div className="columns">
        <div className="column col-5 col-mx-auto">
          <button type="button" className="btn btn-primary btn-block" onClick={onDelete}>Ta bort</button>
        </div>
      </div>
    </div>
  </div>
);

export default LineToolbox;
