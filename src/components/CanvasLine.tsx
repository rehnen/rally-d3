import React, { useRef, useEffect, useState } from 'react';
import {
  Transformer, Line,
} from 'react-konva';

type LineProps = {
  points: Array<number>,
  selected: boolean,
  id: string,
  onSelect: (id: string) => void,
};

const CanvasLine: React.FC<LineProps> = ({
  points,
  selected,
  onSelect,
  id,
}) => {
  const lineRef = useRef() as React.MutableRefObject<any>;
  const transformRef = useRef() as React.MutableRefObject<any>;

  const [scale, setScale] = useState(1);

  useEffect(() => {
    if (selected && transformRef.current) {
      transformRef.current.setNode(lineRef.current);
      transformRef.current.getLayer().batchDraw();
      const scaleX = lineRef.current.attrs?.scaleX || 1;
      const scaleY = lineRef.current.attrs?.scaleY || 1;
      setScale((scaleX + scaleY) / 2);
    }
  });

  return (
    <>
      <Line
        dash={[10 / scale, 10 / scale]}
        bezier
        ref={lineRef}
        points={points}
        draggable
        stroke="teal"
        strokeWidth={3 / scale}
        onMouseDown={() => onSelect(id)}
      />
      {selected
      && (
      <Transformer
        ref={transformRef}
        borderStroke="teal"
        borderStrokeWidth={1.5}
        anchorStroke="teal"
        anchorStrokeWidth={1.5}
        onTransform={() => {
          const scaleX = lineRef.current.attrs?.scaleX || 1;
          const scaleY = lineRef.current.attrs?.scaleY || 1;
          setScale((scaleX + scaleY) / 2);
        }}
      />
      )}
    </>
  );
};

export default CanvasLine;
