import React, { useRef, useEffect, useMemo } from 'react';
import useImage from 'use-image';
import {
  Transformer, Group, Image, Text,
} from 'react-konva';

import SignProps from './SignProps';

const CanvasImage: React.FC<SignProps> = (props) => {
  const {
    onSelect,
    onDragEnd,
    onTransformEnd,
    selected,
    number,
    src,
    x,
    y,
    rotation,
    id,
  } = props;
  const data = {
    selected,
    number,
    src,
    x,
    y,
    rotation,
    id,
  };
  const imageRef = useRef() as React.MutableRefObject<any>;
  const transformRef = useRef() as React.MutableRefObject<any>;
  const [image] = useImage(src);

  useEffect(() => {
    if (selected && transformRef.current) {
      transformRef.current.setNode(imageRef.current);
      transformRef.current.getLayer().batchDraw();
    }
  });
  const startPos = useMemo(() => Math.random() * 100 + 100, []);
  return (
    <>
      <Group
        ref={imageRef}
        draggable
        x={x || startPos}
        y={y || startPos}
        rotation={rotation || 0}
        onDragEnd={({ target: { attrs: { x: x1, y: y1 } } }) => onDragEnd(x1, y1)}
        onTransformEnd={({
          target: {
            attrs: { rotation: r1, x: x1, y: y1 },
          },
        }) => onTransformEnd(x1, y1, r1)}
        onTap={() => onSelect(data)}
        onMouseDown={() => onSelect(data)}
      >
        <Image image={image} width={100} height={80} stroke="teal" strokeWidth={1} />
        <Text
          text={`${number}`}
          x={45}
          y={55}
          width={52}
          fontSize={22}
          stroke="teal"
          align="right"
        />
      </Group>
      {selected
      && (
      <Transformer
        ref={transformRef}
        borderStroke="teal"
        borderStrokeWidth={1.5}
        anchorStroke="teal"
        anchorStrokeWidth={1.5}
        resizeEnabled={false}
      />
      )}
    </>
  );
};

export default CanvasImage;
