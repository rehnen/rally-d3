import React from 'react';


type ToolboxProps = {
  onDelete: () => void,
  number?: number | string,
  onNumberChange: (event: React.ChangeEvent) => void,
};

const Toolbox: React.FC<ToolboxProps> = ({
  onDelete,
  onNumberChange,
  number,
}) => (
  <div className="toolbox container">
    <div className="column col-3 col-mx-auto toolbox-main-area">
      <div className="columns">
        <div className="column col-5 col-mx-auto">
          <button type="button" className="btn btn-primary btn-block" onClick={onDelete}>Ta bort</button>
        </div>
        <div className="column col-5 col-mx-auto">
          <input type="number" className="form-input" onChange={onNumberChange} value={number} />
        </div>
      </div>
    </div>
  </div>
);

export default Toolbox;
