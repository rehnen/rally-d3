/* eslint-disable react/jsx-props-no-spreading */
import React, {
  useState, useEffect, useCallback, useReducer,
} from 'react';
import { Stage, Layer, Line } from 'react-konva';
import { uuid } from 'uuidv4';
import throttle from 'lodash.throttle';
import { KonvaPointerEvent } from 'konva/types/PointerEvents';
import { KonvaEventObject } from 'konva/types/Node';
import SideBar from './SideBar';
import StageToolbox, { Active } from './StageToolbox';
import SignToolbox from './SignToolbox';
import LineToolbox from './LineToolbox';
import CanvasImage from './CanvasSign';
import lineReducer from '../utils/reducers';
import CanvasLine from './CanvasLine';
import useSessionStore from '../utils/useSessionStore';
import SignProps from './SignProps';

type Point = {
  x: number,
  y: number,
};

const unselect = (p: SignProps) => ({ ...p, selected: false });

const RallyStage = () => {
  const [height, setHeight] = useState(1000);
  const [width, setWidth] = useState(1000);
  const [stageOffset, setStageOffset] = useState<Point>({ x: 0, y: 0 });
  const [selectedStageTool, setSelectedStageTool] = useState<Active>('drag');
  const [data, setData] = useSessionStore<Array<SignProps>>('data', []);
  const [
    { lines, isPainting, unfinishedLine },
    lineDispatch,
  ] = useReducer(lineReducer, { lines: [], isPainting: false, unfinishedLine: [] });
  const selectedSign = data.find((p) => p.selected);
  const selectedLine = lines.find((p) => p.selected);

  const onDelete = useCallback(() => {
    setData([...data.filter((p) => !p.selected)]);
  }, [data]);

  const onDeleteLine = useCallback(() => {
    lineDispatch({ type: 'DELETE' });
  }, [lines]);

  const onNumberChange = useCallback((e) => {
    selectedSign.number = e.target.value;
    setData([...data.filter((p) => !p.selected), selectedSign]);
  }, [data]);

  useEffect(() => {
    const onResize = () => {
      setHeight(window.innerHeight - 100);
      setWidth(window.innerWidth - 100);
    };
    window.addEventListener('resize', onResize);
    onResize();

    return () => {
      window.removeEventListener('resize', onResize);
    };
  }, []);

  const unselectAllObjects = () => {
    setData((prev) => [...prev.map(unselect)]);
    lineDispatch({ type: 'UNSELECT' });
  };

  const startPainting = useCallback((event: KonvaEventObject<MouseEvent>) => {
    if (event.target.attrs.id !== 'stage') {
      return;
    }
    unselectAllObjects();
    lineDispatch({ type: 'START' });
  }, []);

  const mouseMove = useCallback(throttle(({ evt }: KonvaPointerEvent) => {
    if (!isPainting) {
      return;
    }
    const { x, y } = stageOffset;
    lineDispatch({ type: 'MOVE', payload: [evt.offsetX - x, evt.offsetY - y] });
  }, 20), [isPainting]);

  const onSelectSign = (a:SignProps) => {
    setData((d) => [...d.filter((p) => p.id !== a.id).map(unselect), { ...a, selected: true }]);
  };

  const onDragEnd = (x:number, y:number) => {
    // setData([...data.filter((p) => !p.selected), { ...selectedSign, x, y }]);
    setData([...data.map((p) => {
      if (!p.selected) {
        return p;
      }
      return {
        ...p,
        x,
        y,
      };
    })]);
  };

  const onTransformEnd = (x:number, y:number, rotation:number) => {
    setData([...data.map((p) => {
      if (!p.selected) {
        return p;
      }
      return {
        ...p,
        x,
        y,
        rotation,
      };
    })]);
  };

  const onSelect = (src: string): void => {
    setData((prevData) => [...prevData.map(unselect), {
      number: Math.max(0, ...prevData.map((d) => d.number)) + 1,
      selected: true,
      id: uuid(),
      src,
      x: 100,
      y: 100,
      rotation: 0,
    }]);
  };

  const onDragMove = useCallback(throttle((event: KonvaEventObject<MouseEvent>) => {
    const { target: { attrs: { x, y } } } = event;
    setStageOffset({ x, y });
  }, 200), []);


  return (
    <div className="container" style={{ height: '100%' }}>
      <div className="columns" style={{ height: '100%' }}>
        <div
          style={{
            display: 'inline-block',
            height: '100%',
          }}
        >
          <SideBar onSelect={onSelect} />
        </div>
        <div className="rehnen-stage" style={{ maxWidth: width, height, border: '1px solid teal' }}>
          {selectedSign
          && (
          <SignToolbox
            number={selectedSign.number}
            onNumberChange={onNumberChange}
            onDelete={onDelete}
          />
          )}
          {selectedLine
          && (
          <LineToolbox
            onDelete={onDeleteLine}
          />
          )}
          {!selectedSign && !selectedLine && (
            <StageToolbox
              active={selectedStageTool}
              onSelect={(val) => setSelectedStageTool(val)}
            />
          )}
          <Stage
            id="stage"
            width={width - 200}
            draggable={selectedStageTool === 'drag'}
            height={height}
            onDragMove={onDragMove}
            onMouseDown={startPainting}
            onTouchStart={unselectAllObjects}
            onMouseUp={() => lineDispatch({ type: 'STOP' })}
            onMouseMove={selectedStageTool === 'draw' ? mouseMove : null}
            onTouchMove={unselectAllObjects}
            onMouseLeave={() => lineDispatch({ type: 'STOP' })}
          >
            <Layer>
              {/* <Line dash={[10, 10]} points={points} stroke="teal" /> */}
              <Line dash={[10, 10]} bezier points={[0, 0, 100, 0, 200, 0, 400, 0, 600, 0, 800, 0, 1000, 0]} stroke="teal" strokeWidth={2} />
              <Line dash={[10, 10]} bezier points={[0, 0, 0, 100, 0, 200, 0, 400, 0, 600, 0, 800, 0, 1000]} stroke="teal" strokeWidth={2} />

              <Line dash={[10, 10]} bezier points={[0, 100, 100, 100, 200, 100, 400, 100, 600, 100, 800, 100, 1000, 100]} stroke="teal" strokeWidth={2} />
              <Line dash={[10, 10]} bezier points={[100, 0, 100, 100, 100, 200, 100, 400, 100, 600, 100, 800, 100, 1000]} stroke="teal" strokeWidth={2} />

              <Line dash={[10, 10]} bezier points={[0, 200, 100, 200, 200, 200, 400, 200, 600, 200, 800, 200, 1000, 200]} stroke="teal" strokeWidth={2} />
              <Line dash={[10, 10]} bezier points={[200, 0, 200, 100, 200, 200, 200, 400, 200, 600, 200, 800, 200, 1000]} stroke="teal" strokeWidth={2} />

              <Line dash={[10, 10]} bezier points={[0, 300, 100, 300, 200, 300, 400, 300, 600, 300, 800, 300, 1000, 300]} stroke="teal" strokeWidth={2} />
              <Line dash={[10, 10]} bezier points={[300, 0, 300, 100, 300, 200, 300, 400, 300, 600, 300, 800, 300, 1000]} stroke="teal" strokeWidth={2} />

              <Line dash={[10, 10]} bezier points={[0, 400, 100, 400, 200, 400, 400, 400, 600, 400, 800, 400, 1000, 400]} stroke="teal" strokeWidth={2} />
              <Line dash={[10, 10]} bezier points={[400, 0, 400, 100, 400, 200, 400, 400, 400, 600, 400, 800, 400, 1000]} stroke="teal" strokeWidth={2} />

              <Line dash={[10, 10]} bezier points={[0, 500, 100, 500, 200, 500, 400, 500, 600, 500, 800, 500, 1000, 500]} stroke="teal" strokeWidth={2} />
              <Line dash={[10, 10]} bezier points={[500, 0, 500, 100, 500, 200, 500, 400, 500, 600, 500, 800, 500, 1000]} stroke="teal" strokeWidth={2} />

              <Line dash={[10, 10]} bezier points={[0, 600, 100, 600, 200, 600, 400, 600, 600, 600, 800, 600, 1000, 600]} stroke="teal" strokeWidth={2} />
              <Line dash={[10, 10]} bezier points={[600, 0, 600, 100, 600, 200, 600, 400, 600, 600, 600, 800, 600, 1000]} stroke="teal" strokeWidth={2} />

              <Line dash={[10, 10]} bezier points={[0, 700, 100, 700, 200, 700, 400, 700, 600, 700, 800, 700, 1000, 700]} stroke="teal" strokeWidth={2} />
              <Line dash={[10, 10]} bezier points={[700, 0, 700, 100, 700, 200, 700, 400, 700, 600, 700, 800, 700, 1000]} stroke="teal" strokeWidth={2} />

              <Line dash={[10, 10]} bezier points={[0, 800, 100, 800, 200, 800, 400, 800, 600, 800, 800, 800, 1000, 800]} stroke="teal" strokeWidth={2} />
              <Line dash={[10, 10]} bezier points={[800, 0, 800, 100, 800, 200, 800, 400, 800, 600, 800, 800, 800, 1000]} stroke="teal" strokeWidth={2} />

              <Line dash={[10, 10]} bezier points={[0, 900, 100, 900, 200, 900, 400, 900, 600, 900, 800, 900, 1000, 900]} stroke="teal" strokeWidth={2} />
              <Line dash={[10, 10]} bezier points={[900, 0, 900, 100, 900, 200, 900, 400, 900, 600, 900, 800, 900, 1000]} stroke="teal" strokeWidth={2} />

              <Line dash={[10, 10]} bezier points={[0, 1000, 100, 1000, 200, 1000, 400, 1000, 600, 1000, 800, 1000, 1000, 1000]} stroke="teal" strokeWidth={2} />
              <Line dash={[10, 10]} bezier points={[1000, 0, 1000, 100, 1000, 200, 1000, 400, 1000, 600, 1000, 800, 1000, 1000]} stroke="teal" strokeWidth={2} />

              {lines.map((line) => (
                <CanvasLine
                  points={line.points}
                  key={line.id}
                  id={line.id}
                  selected={line.selected}
                  onSelect={(id) => lineDispatch({ payload: id, type: 'SELECT' })}
                />
              ))}
              {isPainting
              && (
                <Line dash={[10, 10]} bezier points={unfinishedLine} stroke="teal" strokeWidth={3} />
              )}
              {data.map((p) => (
                <CanvasImage
                  onSelect={onSelectSign}
                  onDragEnd={onDragEnd}
                  onTransformEnd={onTransformEnd}
                  key={p.id}
                  {...p}
                />
              ))}

            </Layer>
          </Stage>
        </div>
      </div>
    </div>

  );
};

export default RallyStage;
