import React from 'react';


type Active = 'drag'| 'draw';

type Props = {
  onSelect: (val: Active) => void,
  active: Active,
};

const StageToolbox: React.FC<Props> = ({
  onSelect,
  active,
}) => (
  <div className="toolbox container">
    <div className="column col-3 col-mx-auto toolbox-main-area">
      <div className="columns">
        <div className="column col-mx-auto">
          <div className="btn-group btn-group-block">
            <button
              type="button"
              className={`btn btn-block${active === 'drag' ? ' active' : ''}`}
              onClick={() => onSelect('drag')}
              style={{ marginLeft: 8 }}
            >
              Muspekare
            </button>
            <button
              type="button"
              className={`btn btn-block${active === 'draw' ? ' active' : ''}`}
              onClick={() => onSelect('draw')}
              style={{ marginRight: 8 }}
            >
              Rita
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export {
  Active,
};

export default StageToolbox;
