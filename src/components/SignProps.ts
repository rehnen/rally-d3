type SignType = {
  number: number,
  id: string,
  src: string,
  selected: boolean,
  rotation?: number,
  x?: number,
  y?: number,
  onDragEnd?: (x: number, y:number) => void,
  onTransformEnd?: (x: number, y:number, rotation: number) => void,
  onSelect?: (sign: SignType) => void
};

export default SignType;
