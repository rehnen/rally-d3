import React, { useCallback } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';


type props = {
  onClick: (nr: string) => void,
  src: string,
  style?: object,
};

const Card = ({ onClick, src }: props) => {
  const callback = useCallback(
    () => {
      onClick(src);
    },
    [],
  );

  const callbackKeydown = useCallback(
    ({ key }) => {
      if (key.toLowerCase() === 'enter') {
        onClick(src);
      }
    },
    [],
  );

  // TODO: Investigate react-lazy-load-image-component to see if it ads a
  // scroll listener for each element and investigate its performance in general
  return (
    <button className="sign-button" onKeyDown={callbackKeydown} onClick={callback} type="button" tabIndex={0}>
      <LazyLoadImage style={{ width: '200px', minHeight: 150 }} alt="img" src={src} />
    </button>
  );
};


export default Card;
