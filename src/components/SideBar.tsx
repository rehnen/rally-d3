import React from 'react';
import SignButton from './SignButton';
import Accordion from './Accordion';

type props = {
  onSelect: (src:string) => void,
};

const SideBar = ({ onSelect }: props) => (
  <div
    className="rehnen-sidebar panel"
    style={{ height: '100%', overflowY: 'auto', overflowX: 'hidden' }}
  >
    <SignButton style={{ width: '100%' }} src="assets/signs/start.jpg" onClick={onSelect} />
    <SignButton style={{ width: '100%' }} src="assets/signs/finish.jpg" onClick={onSelect} />
    <Accordion title="Nybörjare">
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/01.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/02.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/03.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/04.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/05.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/06.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/07.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/08.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/09.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/10.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/11.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/12.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/13.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/14.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/15.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/16.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/17.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/18.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/19.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/20.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/21.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/22.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/23.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/24.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/25.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/26.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/27.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/28.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/29.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/30.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/31.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/32.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/beginner/33.jpg" onClick={onSelect} />
    </Accordion>
    <Accordion title="Fortsättning">
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/01.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/02.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/03.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/04.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/05.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/06.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/07.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/08.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/09.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/10.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/11.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/12.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/13.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/14.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/15.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/16.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/17.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/18.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/continuing/19.jpg" onClick={onSelect} />
    </Accordion>
    <Accordion title="Avanserad klass">
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/01.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/02.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/03.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/04.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/05.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/06.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/07.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/08.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/09.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/10.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/11.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/12.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/13.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/14.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/15.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/16.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/advanced/17.jpg" onClick={onSelect} />
    </Accordion>
    <Accordion title="Mästarklass">
      <SignButton style={{ width: '100%' }} src="assets/signs/master/01.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/master/02.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/master/03.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/master/04.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/master/05.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/master/06.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/master/07.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/master/08.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/master/09.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/master/10.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/master/11.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/master/12.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/master/13.jpg" onClick={onSelect} />
      <SignButton style={{ width: '100%' }} src="assets/signs/master/14.jpg" onClick={onSelect} />
    </Accordion>
  </div>
);

export default SideBar;
