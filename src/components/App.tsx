import React from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import RallyStage from './RallyStage';

import 'spectre.css/dist/spectre.min.css';
import 'spectre.css/dist/spectre-icons.css';
import '../styles/style.css';

const About = () => (
  <div>
    <h1>About</h1>
    <p>Nobody ever has anything interesting to say here</p>
  </div>
);

const PageNotFound = () => (
  <h1>Page Not Found</h1>
);

const App = () => (
  <Router>
    <header className="navbar">
      <section className="navbar-section">
        <a href="/#" className="btn btn-link">Docs</a>
      </section>
      <section className="navbar-center">
        Rally
      </section>
      <section className="navbar-section">
        <a href="http://gitlab.com/rehnen" className="btn btn-link">GitLab</a>
        <a href="http://github.com/rehnen" className="btn btn-link">GitHub</a>
      </section>
    </header>
    <div style={{ height: '100%' }}>
      <Switch>
        <Route exact path="/" component={RallyStage} />
        <Route path="/about" component={About} />
        <Route component={PageNotFound} />
      </Switch>
    </div>
  </Router>
);

export default App;
