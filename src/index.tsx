import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

import './declaration.d';

ReactDOM.render(<App />, document.getElementById('app'));
