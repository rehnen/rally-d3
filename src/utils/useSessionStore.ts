import { useState } from 'react';

type SetterFunction<T>= (v: T) => T;
type SessionStoreHook<T> = [
  T,
  (v: T | SetterFunction<T>) => void,
];

const useSessionStore = <T>(key: string, defaultValue: T): SessionStoreHook<T> => {
  const [stateVal, setStateVal] = useState<T>(() => {
    const val = sessionStorage.getItem(key);
    return val ? JSON.parse(val) : defaultValue;
  });

  const setVal = (value: T | ((v:T) => T)) => {
    const theValue = value instanceof Function ? value(stateVal) : value;
    setStateVal(value);
    sessionStorage.setItem(key, JSON.stringify(theValue));
  };

  return [stateVal, setVal];
};

export default useSessionStore;
