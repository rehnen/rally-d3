import { Reducer } from 'react';
import { uuid } from 'uuidv4';

// TODO add uuid for line each line
// requires lines to have type: Array<{ points: Array<number>, id: string}>
type LineAction = {
  payload?: Array<number> | string,
  type: string,
};

type LineStateType = {
  isPainting: boolean,
  lines: Array<LineType>,
  unfinishedLine: Array<number>,
};

type LineType = {
  points: Array<number>,
  id: string,
  selected: boolean,
};

export const lineReducer: Reducer<LineStateType, LineAction> = (
  state = { isPainting: false, lines: [], unfinishedLine: [] },
  { payload, type },
) => {
  const { lines, unfinishedLine, isPainting } = state;
  switch (type) {
    case 'START':
      return { lines, unfinishedLine: [], isPainting: true };
    case 'MOVE':
      return typeof payload !== 'string'
        ? { lines, unfinishedLine: [...unfinishedLine, ...payload], isPainting } : state;

    case 'STOP':
      return isPainting && unfinishedLine.length > 10 ? {
        lines: [...lines, { points: unfinishedLine, id: uuid(), selected: true }],
        unfinishedLine: [],
        isPainting: false,
      } : { ...state, isPainting: false };
    case 'SELECT':
      return {
        ...state,
        lines: [
          ...lines.map((l) => (l.id !== payload
            ? { ...l, selected: false } : { ...l, selected: true })),
        ],
      };
    case 'DELETE':
      return { ...state, lines: [...lines.filter((l) => !l.selected)] };
    case 'UNSELECT':
      return { ...state, lines: [...lines.map((l) => ({ ...l, selected: false }))] };
    default:
      return state;
  }
};

export default lineReducer;
