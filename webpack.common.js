const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');


module.exports = {
  entry: './src/index.tsx',
  resolve: {
    alias: {
      components: path.resolve(__dirname, './src/components'), 
      assets: path.resolve(__dirname, './src/assets'), 
      signs: path.resolve(__dirname, './src/assets/signs'), 
    },
    extensions: ['.js', '.jsx', '.react.js', '.ts', '.tsx'],
  },
  output: {
    path: path.resolve(__dirname, 'target'),
    filename: 'index_bundle.js',
  },
  devtool: 'inline-source-map',
  devServer: {
    stats: 'errors-only',
    historyApiFallback: true,
  },
  module: {
    rules: [
      { test: /(\.ts?$|\.tsx?$)/, loader: 'awesome-typescript-loader' },
      {
        test: /(\.js$|\.jsx$)/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env'],
          // eslint-disable-next-line
          plugins: ['@babel/plugin-proposal-object-rest-spread'],
        },
      },
      {
        test: /\.(png|jpg|jpeg)$/,
        loader: 'file-loader',
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
    }),
    new CopyPlugin([
      { from: 'src/assets', to: 'assets'},
    ]),
  ],
};
