module.exports = {
  "extends": ["airbnb-typescript", "plugin:import/typescript"],
  "env": {
    "jest": true,
  },
  parserOptions: {
    project: './tsconfig.json',
  },
  "rules": {
    "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx", ".ts", ".tsx"] }],
    "import/no-named-as-default": 0,
    "import/no-named-as-default-member": 0,
    "react/prop-types": 0,
    "jsx-a11y/no-static-element-interactions": 0,
    "jsx-a11y/click-events-have-key-events": 0,
    "import/no-extraneous-dependencies": 0
  },
  plugins: ['import'],
  settings: {
    "import/parsers": {
      '@typescript-eslint/parser': [".ts", ".tsx"],
    },
    "import/resolver": {
      alias: {
        map: [
          ['assets', './src/assets'],
          ['signs', './src/assets/signs'],
        ]
      },
      typescript: {
        
      } // this loads <rootdir>/tsconfig.json to eslint
    },
  },
  globals: {
    'document': false,
    'window': false
  }
};