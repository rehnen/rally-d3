const path = require('path');
const fs = require('fs');

const rootDirPath = path.resolve(__dirname, './src/assets/signs');

const readReq = (directoryPath) => {
  const files = fs.readdirSync(directoryPath);
  if(!files) {
    console.log(files);
    return;
  } else {
    const childen = files.map(f => {
      const filePath = path.resolve(directoryPath, f);
      if (fs.statSync(filePath).isDirectory()) {
        return readReq(filePath);
      } else {
        return filePath;
      }
    });
    return childen;
  }
}
const signs = readReq(rootDirPath);

fs.writeFileSync(path.resolve(rootDirPath, 'signs.json'), JSON.stringify(signs));
